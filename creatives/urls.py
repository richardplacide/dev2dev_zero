from django.conf.urls import url
from creatives import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
]
