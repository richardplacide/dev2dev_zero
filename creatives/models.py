from django.db import models

class Domain(models.Model):
    domain = models.CharField(max_length=100)

    def __str__(self):
        return self.domain

class Skill(models.Model):
    domain = models.ForeignKey('Domain', related_name='SkillListDomain')
    skillname = models.CharField(max_length=100)

    def __str__(self):
        return self.skillname

class Location(models.Model):
    location = models.CharField(max_length=150)

    def __str__(self):
        return self.location

class Creative(models.Model):
    username = models.CharField(max_length=100)
    first = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    joined = models.DateField(auto_now_add=True)
    email = models.CharField(max_length=100)
    github = models.CharField(max_length=100)
    www = models.CharField(max_length=100, blank=True)
    mylocation = models.ForeignKey('Location', related_name="mylocation")
    summary = models.CharField(max_length=500)
    mentorpoints = models.IntegerField(default=0)
    communitypoints = models.IntegerField(default=0)

    def __str__(self):
        return self.email

class AdObjective(models.Model):
    objective = models.CharField(max_length=100)

    def __str__(self):
        return self.objective

class Ad(models.Model):
    owner = models.ForeignKey('Creative', related_name='owner')
    created = models.DateField(auto_now_add=True)
    title = models.CharField(max_length=100)
    skill1 = models.ForeignKey('Skill', related_name='adskill1')
    skill1 = models.ForeignKey('Skill', related_name='adskill2', blank=True)
    objective = models.ForeignKey('AdObjective', related_name='ads_objective')
    content = models.CharField(max_length=500)
    githubrepo = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created']

class Level(models.Model):
    level = models.IntegerField()

    def __str__(self):
        return self.level

class RegisteredSkill(models.Model):
    regid = models.IntegerField(blank=True, null=True)
    creative = models.ForeignKey('Creative', related_name="registeredcreative")
    skill = models.ForeignKey('Skill', related_name="registeredskill")
    level = models.ForeignKey('Level', related_name="skill_level")

    def __str__(self):
        return self.skill
