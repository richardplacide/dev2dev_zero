from django.shortcuts import render

# Create your views here.
def home(request):
    return render(
        request,
        'home.html',
        {'home_data': 'Some data for home...'}
    )
