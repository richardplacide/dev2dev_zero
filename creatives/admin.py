from django.contrib import admin
from .models import Creative, Domain, Skill, Location, AdObjective, Ad, RegisteredSkill

admin.site.register(Creative)
admin.site.register(Domain)
admin.site.register(Skill)
admin.site.register(Location)
admin.site.register(AdObjective)
admin.site.register(Ad)
admin.site.register(RegisteredSkill)
